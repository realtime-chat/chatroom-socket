const io = require("socket.io")(8900, {
    cors: {
      origin: "http://localhost:3000",
    },
  });
  
  
  io.on("connection", (socket) => {
    //when ceonnect
    console.log("connected.");
  
    socket.on("join", ({ roomId, displayName }) => {
        io.emit("userJoin", {
            roomId,
            displayName
        });
    });

    socket.on("leave", ({ roomId, displayName }) => {
        io.emit("userLeave", {
            roomId,
            displayName
        });
    });
  
    socket.on("createRoom", ({ roomId, title, creatorName }) => {
        io.emit("getRoom", {
            _id: roomId, 
            title,
            members: [ creatorName ]
        });
    });
  
    socket.on("sendMessage", ({ roomId, sender, msg, createdAt}) => {
        io.emit(roomId, {
            sender,
            msg,
            createdAt
        });
    });
  });